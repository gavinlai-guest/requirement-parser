requirements-parser (0.11.0-2) unstable; urgency=medium

  * d/control: Build-Depends on python3-setuptools (Closes: #1080794)

 -- Gavin Lai (賴建宇) <gavin09@gmail.com>  Fri, 06 Sep 2024 08:33:35 +0800

requirements-parser (0.11.0-1) unstable; urgency=medium

  * New upstream version
  * d/tests: Add pytest
  * d/salsa-ci: Add default pipeline

 -- Gavin Lai (賴建宇) <gavin09@gmail.com>  Thu, 29 Aug 2024 00:18:23 +0800

requirements-parser (0.9.0-0.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Update Breaks+Replaces (Closes: #1072390)

 -- Alexandre Detiste <tchet@debian.org>  Sun, 02 Jun 2024 13:30:11 +0200

requirements-parser (0.9.0-0.3) unstable; urgency=medium

  * Non-maintainer upload.

  [ SZ Lin (林上智) ]
  * Fix broken package naming (Closes: #1019735)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 31 May 2024 16:37:33 +0200

requirement-parser (0.9.0-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Temporary rollback to existing package name in autopkgtest too.

 -- Alexandre Detiste <tchet@debian.org>  Sat, 25 May 2024 13:00:26 +0200

requirement-parser (0.9.0-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream version (Closes: #1010042)
  * Replace nose(2) by pytest (Closes: #1018612)
  * Standards-Version: 4.7.0 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Remove obsolete field Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove patch applied upstream

 -- Alexandre Detiste <tchet@debian.org>  Sat, 25 May 2024 01:57:01 +0200

requirement-parser (0.5.0+git20220420.1ce9236-2) unstable; urgency=medium

  * d/copyright
    - Update upstream copyright
  * d/control
    - Bump Standards-Version to 4.6.2

 -- Gavin Lai (賴建宇) <gavin09@gmail.com>  Wed, 08 Feb 2023 23:54:48 +0800

requirement-parser (0.5.0+git20220420.1ce9236-1) unstable; urgency=medium

  [ Gavin Lai (賴建宇) ]
  * New upstream version (Closes: #1010042)
  * d/control:
    - Replace python3-nose to python3-nose2 in Build-Depends (Closes: #1018612)
    - Bump Standards-Version to 4.6.1.0
    - Add Build-Depends to support PEP 517
  * d/patches
    - Add Fix-lintian-unknown-file-in-python-module-directory patch

  [ SZ Lin (林上智) ]
  * Replace the inactive maintainer with an active one
  * Fix broken package naming (Closes: #1019735)

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 03 Jan 2023 21:01:30 +0800

requirement-parser (0.2.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Source-only upload for testing migration.
  * debian/control: Bump Standards-Version to 4.5.1.

 -- Boyuan Yang <byang@debian.org>  Thu, 24 Dec 2020 14:26:24 -0500

requirement-parser (0.2.0-1) unstable; urgency=low

  [ Alvin Chen ]
  * Initial release. Closes: #958456

  [ SZ Lin (林上智) ]
  * d/control:
    - Add VCS information
    - Add uploader column
  * d/copyright:
    - Add copyright information

 -- SZ Lin (林上智) <szlin@debian.org>  Fri, 08 May 2020 17:40:29 +0800
